<?php

namespace App\Controller;

use App\Entity\BD;
use App\Repository\BDRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class BDController extends AbstractController
{
    /**
     * @Route("/bd", name="b_d")
     */
    public function Form( ObjectManager $manager, BDRepository $Repo)
    {

        $bd = new BD();
        $form = $this->createForm(BDFormType::class, $bd);
        $form->handleRequest($Repo);

        if($form->isSubmitted() && $form->isValid()) {
            $imgPath = $uploader->upload($bd->getUploadImage());
            $bd->setImage($imgPath);

            $bd->getTitle();
            $bd->getAuthor();
            $bd->setParution(new \DateTime());
           // ->setOwner($this->getUser());
            $manager->persist($bd);
            $manager->flush();
            return $this->redirectToRoute('home');
        }


        return $this->render('bd/index.html.twig', [
            'bd' => $Repo->findAll(),
        ]);
    }
}
