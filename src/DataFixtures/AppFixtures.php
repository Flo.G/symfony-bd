<?php

namespace App\DataFixtures;

use App\Entity\BD;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;


class AppFixtures extends Fixture{  

    public function load(ObjectManager $manager)
{
    $bd1 = new Bd();

    $bd1->setAuthor('Goscinny')->setDescription('Merveilleuse BD')->setTitle('les 12 travaux dAstérix')->setParution(new \DateTime('1978-01-01'));
    $manager->persist($bd1);
    $bd2 = new Bd();
    $bd2->setAuthor('Zozo')->setDescription('Best Comics Ever')->setTitle('Braaaaa')->setParution(new \DateTime('1991-08-01'));
    $manager->persist($bd2);

    $manager->flush();
    }
}
