<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Process\Process;

class BdControllerTests extends WebTestCase
{

    public function setUp()
    {
        $process = new Process(['php', 'bin/console', 'do:fi:lo']);
        $process->run();
    }

    public function testPicturesDisplay()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/bd');

        $this->assertResponseIsSuccessful();
        $this->assertCount(4, $crawler->filter('.card m-2'));
    }

    /**
     * @Route("/add", name="form_add")
     */
    public function testAddBd()
    {
        $client = static::createClient([], [
            'PHP_AUTH_USER' => 'mail@mail.com',
            'PHP_AUTH_PW' => '1234'
        ]);
        $crawler = $client->request('GET', '/add');

        $this->assertResponseIsSuccessful();
        //On récupère le formulaire en se basant sur le texte du button submit
        $form = $crawler->selectButton('Post')->form();
        //On rempli les différents champs du formulaire
        $form['BD[title]'] = 'test';
        $form['BD[author]'] = 'test';
        $form['BD[description]'] = 'test';
        //On submit le formulaire
        $client->submit($form);
        //On vérifie si une redirection est bien déclenchée vers la 
        //page d'accueil, indiquant le succès du formulaire
        $this->assertResponseRedirects('/');
        //On récupère le UserRepository "manuellement"
        $repo = static::$container->get('App\Repository\BDRepository');
        //On vérifie qu'un nouvel user a bien été ajouté dans la bdd
        $this->assertCount(2, $repo->findAll());

    }
}